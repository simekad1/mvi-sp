Ukolem semestralni prace bylo seznamit se s instance segmentation a vyzkouset a porovnat soucasne metody instance segmentation z hlediska presnosti a rychlosti.

Nutne mit:

Pytorch 1.3+

OpenCV4+

Linux

COCOAPI

https://github.com/facebookresearch/detectron2 #na ukazkove kody pro video a instance si stahne model sam pomoci kodu

data:
http://cocodataset.org/#download 2015 nebo 2017 train a val only obrazky a anotace
vychozi modely: moc velke 200-300MB

pro vetsi train jsem pouzival detectron2/tools/train_net.py, to same u ostatnich siti pro jejich test jejich test scripty mirne upravene pro konkretni dataset (prepsani path), vetsina jich je pripravena pro COCO

https://github.com/chengyangfu/retinamask/

https://github.com/dbolya/yolact            #Yoloact ma i pekne video prehravani v real-time

https://github.com/ShuLiu1993/PANet

https://github.com/zjhuang22/maskscoring_rcnn

prilozene soubory:

detector2inference.ipynb #jednoducha testovaci inference a video

detector2train.ipynb #priuceni noveho objektu, dataset se stahne samostatne

pretrained.ipynb #pokus ve kterem jsem zkousel implementovat model mask R-CNN pomoci tensorflow, funguje, a naucil jsem se na tom hodne o R-CNN avsak dale jsem ho neuzival (pomaly), pouziva metody z knihovny https://github.com/matterport/Mask_RCNN

*.yaml configy pro resnet50 FPN nahradit configs/Base-RCNN-FPN.yaml a pritrenovat, avsak ke zlepsenim to nevede

Benchmarky jsou mereny na spped v FPS a mean Average Precission mAP v mask caregory (presnost urceni masky obejktu) 

Vsechny kernely pro testy vychazeji z maskrcnn-benchmarku https://github.com/facebookresearch/maskrcnn-benchmark kterymi se ridi vsechny zminene knihovny

napr. https://github.com/facebookresearch/detectron2/blob/d5dcbc3037a321238e5d539a051ceb46dd015cf2/detectron2/evaluation/coco_evaluation.py